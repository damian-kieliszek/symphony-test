$(document).ready(function () {

  var AppModule = (function (window, $) {

    var cartValue;

    var validateConfig = {
      rules: {
        firstName: {
          required: true,
          rangelength: [2, 64]
        },
        lastName: {
          required: true,
          rangelength: [2, 64]
        },
        email: {
          required: true,
          email: true
        },
        phone: {
          required: false,
          phoneUS: true
        },
        address: {
          required: true
        },
        addressCont: {
          required: false
        },
        zipCode: {
          required: true,
          zipcodeUS: true
        },
        state: {
          required: true
        },
        city: {
          required: true
        },
        country: {
          required: true
        },
        cardNumber: {
          required: true,
          creditcard: true
        },
        cardHolder: {
          required: true,
          rangelength: [2, 128]
        },
        cvc: {
          required: true,
          digits: true,
          minlength: 3,
          maxlength: 3
        }
      },
      errorClass: 'form--error'
    }

    function setCartValue() {
      if (window.localStorage) {
        cartValue = window.localStorage.getItem('cartValue');
        if (cartValue) {
          $('#cart span').text(cartValue);
        } else {
          window.localStorage.setItem('cartValue', 0);
        }
        setProductValue();
      }
    }

    function updateCart() {
      cartValue = +window.localStorage.getItem('cartValue') + 1;
      window.localStorage.setItem('cartValue', cartValue);
      $('#cart span').text(cartValue);
    }

    function bindNotify() {
      $(document).on('click', '#notify', function () {
        var randomId = 'rewrwe21';
        var $randomElement = $('<div>').attr('id', randomId).addClass('message message__header message--info').text('test notification').hide();
        $('#messages').append($randomElement);
        $randomElement.fadeIn('slow', function () {
          $(this).fadeOut('slow', function () {
            $(this).remove();
          });
        });
      });
    }

    function bindNavigation() {
      $('#navigation__toggle').on('click', function () {
        $(this).toggleClass('navigation__hamburger--active');
      });
    }

    function bindAddToCart() {
      $('#addtocart').on('click', function () {
        updateCart();
      });
    }

    function bindResize() {
      $(window).resize(function () {
        setGridOffset();
      });
    }

    function setGridOffset() {
      var navigationHeight = $('#navigation .navigation__list').height();
      var $firstGridContainer = $('#grid');
      if ($firstGridContainer && navigationHeight) {
        $firstGridContainer.css('padding-top', navigationHeight);
      }
    }

    function setProductValue() {
      var $productValue = $('#productValue');
      if ($productValue) {
        $productValue.text(window.localStorage.getItem('cartValue'));
      }
    }

    function bindInputBlur() {
      $('input').on('blur', function () {
        if ($("#orderForm")) {
          if ($("#orderForm").valid()) {
            $('#submit').prop('disabled', false).removeClass('form__input--disabled');
          } else {
            $('#submit').prop('disabled', 'disabled').addClass('form__input--disabled');
          }
        }
      });
    }

    function bindEvents() {
      bindNotify();
      bindNavigation();
      bindAddToCart();
      bindResize();
      bindInputBlur();
    }

    function init() {
      setGridOffset();
      setCartValue();
      bindEvents();
      $('#orderForm').validate(validateConfig);
    }

    return {
      init: init
    }

  })(window, jQuery);

  AppModule.init();

});
