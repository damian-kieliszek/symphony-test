/* 0. import required libs */

var gulp = require('gulp'),
  concat = require('gulp-concat'),
  plumber = require('gulp-plumber'),
  notify = require('gulp-notify'),
  rename = require('gulp-rename'),
  uglify = require('gulp-uglify'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps');

/* 1. baseDirs: baseDirs for the project */

var baseDirs = {
  assets: 'src/AppBundle/Resources/assets/',
  public: 'src/AppBundle/Resources/public/',
  bundles: 'web/bundles/app/'
};

/* 2. routes: object that contains the paths */

var routes = {
  scripts: {
    base: baseDirs.assets + 'js/',
    js: [
      baseDirs.assets + 'js/*.js'
    ],
    jsmin: baseDirs.bundles + 'js/'
  },
  styles: {
    scss: baseDirs.assets + 'gfx/scss/*.scss',
    css: baseDirs.public + 'css/'
  }
};

/* 3. Scripts (js) copy, minify and concat into a single file. */

gulp.task('scripts', function () {
  return gulp.src(routes.scripts.js)
    .pipe(plumber({
      errorHandler: notify.onError({
        title: "Error: Babel and Concat failed.",
        message: "<%= error.message %>"
      })
    }))
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest(routes.scripts.jsmin))
    .pipe(notify({
      title: 'JavaScript Minified and Concatenated!',
      message: 'your js files has been minified and concatenated.'
    }));
});

/* 4. SCSS - compiling scss and css files and other actions (autoprefixer, sourcemaps etc.) */

gulp.task('styles', function () {
  return gulp.src(routes.styles.scss)
    .pipe(plumber({
      errorHandler: notify.onError({
        title: "Error: Compiling SCSS.",
        message: "<%= error.message %>"
      })
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(autoprefixer('last 3 versions'))
    .pipe(rename('main.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(routes.styles.css))
    .pipe(notify({
      title: 'SCSS Compiled and Minified succesfully!',
      message: 'scss task completed.'
    }));
});

/* 5. Main gulp tasks */

gulp.task('dev', ['scripts', 'styles']);

gulp.task('default', function () {
  gulp.start('dev');
});
